* oshi.at GNU/linux CLI uploader

** Descrition
This is a very tiny /bash/ command-line uploader tool for GNU/linux operating system. It reliably works in /GNU Emacs e-shell/ mode. It will probably work in freaky WSL or cygwin.

** Requirements
The script requires just:\\

*cURL* for for transferring data using various network protocols,

*stat* for checking the file size.

** Usage
Syntax is simple:

#+begin_src 
oshi.sh <filename> <expiration days>
#+end_src

After =<expiration days>= the file will be deleted from the server. Note, that =0= will not work, as unlimited uploads are not allowed.

After uploading you will get two *urls* - one is for downloading, one for managing the upload (for deleting, change expiration…).


That's it.


