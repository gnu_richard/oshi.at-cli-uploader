#!/bin/bash

echo
echo "▬▮ oshi.at using cURL ▮▬"

if [ $# -lt 2 ]; then
    echo 1>&2 "$0: not enough arguments"
    echo "It must be: oshi.sh <filename> <expiration days>"
    exit 2
elif [ $# -gt 2 ]; then
    echo 1>&2 "$0: too many arguments"
    echo "It must be: oshi.sh <filename> <expiration days>"
    exit 2
fi

echo -n "Using cURL located in: "
which curl
if [ $? -gt 0 ]; then
    echo 1>&2 "! cURL package not installed in the system !"
    exit 2
fi

echo -n "Using stat located in: "
which stat
if [ $? -gt 0 ]; then
    echo 1>&2 "! stat is missing, I won't check the file size !"
else
    if [ `stat -c%s $1` -gt 5000000000 ]; then
	echo 1>&2 "! Your file is too big (more than 5000 MB) !"
	exit 2
    fi
fi

echo -n "Expire in days: "
echo $2
let minutes=$2*24*60
echo "Uploading: "
curl https://oshi.at -F f=@$1 -F expire=$minutes -# | tee /dev/null 
